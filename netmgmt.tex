\documentclass{beamer}
\usetheme{Madrid}
\setbeamertemplate{navigation symbols}{}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{listings}

\title[Networking with open source]{Network management with open source technologies}
\author{Mgr. Tomáš Szaniszlo}
\institute[FI MU]{Faculty of Informatics, Masaryk University, Brno, Czech Republic}

\begin{document}

\frame{\titlepage}


\begin{frame}
\frametitle{Overview}

Examples of some selected useful tools:
\begin{itemize}
    \item \texttt{ping}, \texttt{fping}
    \item \texttt{traceroute}
    \item \texttt{mtr}
    \item smokeping
    \item Arpwatch
    \item NDPMon
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{Overview}

\begin{itemize}
    \item \textbf{Motivation}: troubleshooting packet loss and slow connection
    \item \textbf{Methods}: Analysis of ICMP/TCP packets with varying TTL
    \item \textbf{Tools}: \texttt{ping}, \texttt{fping}, \texttt{traceroute}, \texttt{tcptraceroute}, \texttt{mtr}, Smokeping
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{Network layer latency}

Measuring latency:

\begin{itemize}
    \item basic parameter is RTT (Round-Trip Time)
    \item easy to measure
    \item RTT is a key quality metric for network connections
\end{itemize}

\medskip

RTT can be large even in mission critical real world scenarios. Really large...

\pause

\medskip

Current RTT from Earth to cosmic probe Opportunity on Mars is 10.2 minutes: \url{https://www.wolframalpha.com/input/?i=Mars+current+distance+from+Earth}.

\end{frame}

\begin{frame}[fragile]
\frametitle{RTT and packet loss measurement}
\framesubtitle{IPoAC: IP over Avian Carriers}

\url{https://www.blug.linux.no/project/rfc1149/}

{ \tiny
\begin{semiverbatim}
Script started on Sat Apr 28 11:24:09 2001
vegard@gyversalen:~$ /sbin/ifconfig tun0
tun0      Link encap:Point-to-Point Protocol
          inet addr:10.0.3.2  P-t-P:10.0.3.1  Mask:255.255.255.255
          UP POINTOPOINT RUNNING NOARP MULTICAST  MTU:150  Metric:1
          RX packets:1 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0
          RX bytes:88 (88.0 b)  TX bytes:168 (168.0 b)

vegard@gyversalen:~$ ping -i 900 10.0.3.1
PING 10.0.3.1 (10.0.3.1): 56 data bytes
64 bytes from 10.0.3.1: icmp_seq=0 ttl=255 time=6165731.1 ms
64 bytes from 10.0.3.1: icmp_seq=4 ttl=255 time=3211900.8 ms
64 bytes from 10.0.3.1: icmp_seq=2 ttl=255 time=5124922.8 ms
64 bytes from 10.0.3.1: icmp_seq=1 ttl=255 time=6388671.9 ms

--- 10.0.3.1 ping statistics ---
9 packets transmitted, 4 packets received, 55% packet loss
round-trip min/avg/max = 3211900.8/5222806.6/6388671.9 ms
vegard@gyversalen:~$ exit

Script done on Sat Apr 28 14:14:28 2001
\end{semiverbatim}
}

% https://tex.stackexchange.com/questions/167719/how-to-use-background-image-in-latex
% https://en.wikipedia.org/wiki/File:Rock_dove_-_natures_pics.jpg
\tikz[remember picture,overlay] \node[opacity=0.3,inner sep=0pt] at (4.9cm, 4.05cm){\includegraphics[width=0.8\paperwidth]{pigeon-small.jpg}};

\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{Packet hop count}

\begin{itemize}
	\item layer 3: packets travel through routers
	\item each router = one hop
    \item safeguard against looping packets
    \item both IPv4 and IPv6 have dedicated fields: decreased by one by each router
\end{itemize}

\medskip

Side effect! ability to measure partial route metrics.

\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{IPv4 packet header}

\begin{center}
    \begin{figure}
        \includegraphics[width=\textwidth]{ipv4-header.png}
    \end{figure}
\end{center}

\url{https://en.wikipedia.org/wiki/IPv4\#Header}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{IPv6 packet header}

\begin{center}
    \begin{figure}
        \includegraphics[width=\textwidth]{ipv6-header.png}
    \end{figure}
\end{center}

\url{https://en.wikipedia.org/wiki/IPv6_packet\#Fixed_header}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{\texttt{ping}}

\begin{itemize}
    \item measurement of latency
    \item based on ICMPv4 (\texttt{ping -4}) or ICMPv6 (\texttt{ping -6})
    \item basic statistics
    \item one target at a time
\end{itemize}

\medskip

Options:
\begin{itemize}
    \item variable payload size (\texttt{ping -s 56})
    \item specified TTL (\texttt{ping -t 20})
    \item flood mode (\texttt{ping -f})
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{\texttt{ping}}

Can detect various problems:

\begin{itemize}
    \item packet loss
    \item high latency/RTT
    \item high jitter
    \item packet duplication
    \item packet reordering
    \item packet damage
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{\texttt{fping}}

\begin{itemize}
    \item similar as \texttt{ping}
    \item allows multiple targets
    \item parallel processing
    \item scripting friendly
\end{itemize}

\medskip

Options:
\begin{itemize}
    \item load targets from file (\texttt{fping -f})
    \item specify whole network (\texttt{fping -g 192.168.0.0/24})
    \item network outage time (\texttt{fping -o})
\end{itemize}

For a more statistically advanced version see \texttt{oping}.
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{Interesting \texttt{fping} examples}

\begin{block}{LOL, \texttt{ping} on steroids!}
    \begin{semiverbatim}
    fping -lol 192.168.0.1
    \end{semiverbatim}

    Allows you to see running summary and total outage.
\end{block}

\pause
\bigskip

\begin{block}{Be careful what you wish for!}
    \begin{semiverbatim}
    \$ fping -p 1 192.168.0.1

    fping: these options are too risky for mere mortals.

    fping: You need i >= 1, p >= 20, r < 20, and t >= 50
    \end{semiverbatim}
\end{block}

\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{\texttt{traceroute}, \texttt{tcptraceroute}}

\begin{itemize}
    \item probing target with different TTL or hop count
    \item options: TCP (\texttt{-T}), UDP (\texttt{-U}), ICMP (\texttt{-T})
\end{itemize}

Sometimes magic is hidden in such basic Linux commands:

\medskip

\textit{Probe packets are udp datagrams with so-called  "unlikely"  destination
ports.   The "unlikely" port of the first probe is 33434, then for each
next probe it is incremented by one. Since the ports are expected to be
unused,  the destination host normally returns "icmp unreach port" as a
final response.  (\textbf{Nobody knows what happens when some application  lis‐
tens for such ports, though}).
}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{\texttt{mtr}}

\begin{itemize}
    \item a combination of \texttt{ping} and \texttt{traceroute} tools
    \item summary report (\texttt{mtr -r})
    \item specific statistic information (\texttt{mtr -o [LDRSNBAWVGJMXI]})
    \item additional information about IPs (\texttt{mtr -y n})
    \item can also use TCP and UDP (may improve results)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{RTT and packet loss measurement}
\framesubtitle{Smokeping}

\begin{itemize}
    \item \url{https://oss.oetiker.ch/smokeping/} (development started in 2001)
    \item permanent \texttt{ping} monitoring
    \item commonly available as a package
    \item latency visualisation
    \item supports different plugins (not only ICMP, but HTTP, DNS, SSH, \dots)
    \item alerting
\end{itemize}
\end{frame}

%%%%%%%%%% ARPWATCH %%%%%%%%%%


\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{Overview}

\begin{itemize}
    \item \textbf{Motivation}: monitoring IPv4 addresses usage
    \item \textbf{Methods}: ARP packets capture and analysis
    \item \textbf{Tools}: Arpwatch, access switch ARP monitoring and DHCP snooping
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{ARP: Address Resolution Protocol}
\begin{itemize}
    \item very old protocol (1982)
    \item \url{https://tools.ietf.org/html/rfc826}
    \item translation of internet addresses to link layer addresses
    \item DAD: Duplicate Address Detection
    \item most commonly IPv4 and Ethernet
    \item RARP = Reverse ARP
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{ARP: Address Resolution Protocol}
Example communication:

\bigskip

192.168.0.1 wants to communicate with 192.168.0.2.\footnote{
This can be simulated using \texttt{arping -I eth0 -c 1 192.168.0.2}} \\

\begin{itemize}
    \item \textbf{Who has 192.168.0.2? Tell 192.168.0.1.} \\
        { \small
            Sender MAC: 54:27:58:83:39:c1, Sender IP: 192.168.0.1 \\
            Target MAC: 00:00:00:00:00:00, Target IP: 192.168.0.2
        }
    \item \textbf{192.168.0.2 is at 1c:3e:84:db:db:b4.} \\
        { \small
            Sender MAC: 1c:3e:84:db:db:b4, Sender IP: 192.168.0.2 \\
            Target MAC: 54:27:58:83:39:c1, Target IP: 192.168.0.1
        }
\end{itemize}

Retrieved information is cached.
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{Arpwatch}

\begin{itemize}
    \item \url{http://ee.lbl.gov/}
    \item tool created in 1992 at Berkeley
    \item latest version from 2006
    \item packaged in most distributions (\texttt{arpwatch})
\end{itemize}

Detection features:
\begin{itemize}
    \item ARP spoofing
    \item IP address takeover/collisions
    \item flip-flop
    \item DAD DoS
    \item station activity: new and changes
\end{itemize}

See \texttt{https://linux.die.net/man/8/arpwatch} for full list.
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{MAC--IPv4 pairing}

\begin{itemize}
    \item syslog/mails can be processed
    \item database of MAC and IPv4 pairs with timestamps
    \item overview of MAC and IPv4 usage
    \item add switch (even L2!) bridge tables: \\
        \textbf{device localization to switch \& port}
    \item very valuable network information
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Monitoring IPv4 address space}
\framesubtitle{Arpwatch examples}

\begin{block}{Example arpwatch command}
\begin{semiverbatim}
arpwatch -i vlan48 -f vlan48.dat -N \\
    -m arpwatch@example.net -z 169.254.0.0 255.255.0.0
\end{semiverbatim}
\end{block}

Monitor ARP packets on interface vlan48 while ignoring bogons (non-local
addresses) and additionally ignore 169.254.0.0/16 (APIPA). ARP database
(triples MAC address, IPv4 address, unix timestamp) will be stored in
file vlan48.dat and reports will be sent to arpwatch@example.net besides
logging it to syslog unconditionally.

\medskip

See \texttt{arpwatch(8)} for more information.
\end{frame}

\begin{frame}[fragile]
\frametitle{Monitoring IPv4 address space}
\framesubtitle{Arpwatch examples}

\small
% WTF, it shows only two dashes instead of three
\begin{semiverbatim}
Sep 30 10:59:59 ares1 arpwatch: changed ethernet address
    147.251.48.195 1c:6f:65:37:48:6a (00:13:d4:d9:b6:5a) vlan48

---

Subject: changed ethernet address (minos.fi.muni.cz) vlan48

            hostname: minos.fi.muni.cz
          ip address: 147.251.48.195
           interface: vlan48
    ethernet address: 1c:6f:65:37:48:6a
     ethernet vendor: GIGA-BYTE TECHNOLOGY CO.,LTD.
old ethernet address: 00:13:d4:d9:b6:5a
 old ethernet vendor: ASUSTek COMPUTER INC.
           timestamp: Sunday, September 30, 2018 10:59:59 +0200
  previous timestamp: Thursday, December 9, 2010 17:05:22 +0200
               delta: 2851 days
\end{semiverbatim}

\end{frame}

\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{Access switch ARP monitoring}
% http://www.h3c.com.hk/Technical_Support___Documents/Technical_Documents/Routers/H3C_SR8800_Series_Routers/Command/Command/H3C_SR8800_CR-Release3347-6W103/10/201211/761788_1285_0.htm

Basic access switch abilities (for HP Comware):

\begin{block}{Log/block excessive ARP replies per source MAC}
\begin{semiverbatim}
arp anti-attack source-mac threshold 5

arp anti-attack source-mac filter
\end{semiverbatim}
\end{block}

\begin{block}{Block excessive ARP requests from a host}
\begin{semiverbatim}
arp source-suppression limit 5

arp source-suppression enable
\end{semiverbatim}
\end{block}

ARP detection -- more secure solution, but without DHCP also tedious.
% http://www.h3c.com.hk/Technical_Support___Documents/Technical_Documents/Switches/H3C_S12500_Series_Switches/Configuration/Operation_Manual/H3C_S12500_CG-Release7128-6W710/10/201301/772682_1285_0.htm#_Toc341177733

\end{frame}

\begin{frame}
\frametitle{Monitoring IPv4 address space}
\framesubtitle{Access switch DHCP snooping}
% http://www.h3c.com.hk/Technical_Support___Documents/Technical_Documents/Switches/H3C_S5120_Series_Switches/Configuration/Operation_Manual/H3C_S5120-SI_CG-Release_1101-6W105/201108/723582_1285_0.htm

Basic access switch abilities (for HP Comware):

\begin{block}{Block ARP/general traffic not in match with DHCP}
\begin{semiverbatim}
dhcp-snooping information enable

interface te1/1/1

\ \ \ \ dhcp snooping trust

dhcp-snooping
\end{semiverbatim}
\end{block}

\end{frame}


%%%%%%%%%% NDPMON %%%%%%%%%%


\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{Overview}

\begin{itemize}
    \item \textbf{Motivation}: monitoring IPv6 addresses usage
    \item \textbf{Methods}: NDP packets capture and analysis
    \item \textbf{Tools}: NDPMon, RA guard
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDP: Neighbor Discovery Protocol}
\begin{itemize}
    \item a subset of ICMPv6, defined in 1996
    \item \url{https://tools.ietf.org/html/rfc4861}
    \item discovery of information about routers, neighbors and the network
        \begin{itemize}
            \item translation of IPv6 addresses to link layer addresses
            \item IPv6 router discovery
            \item DAD
        \end{itemize}
    \item IPv6-specific protocol
    \item IND = Inverse Neighbor Discovery
    \item SEND = Secure Neighbor Discovery
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDP: Neighbor Discovery Protocol}
Example router discovery communication: \\
\bigskip
fe80::225:90ff:fed6:6ab0 is connecting to network using RA and wants to find out information about it.\footnote{
This can be simulated using \texttt{rdisc6 eth0}} \\

\begin{itemize}
    \item \textbf{Router Solicitation by fe80::225:90ff:fed6:6ab0.} \\
        { \small
            Sender MAC: 00:25:90:d6:6a:b0, Sender IP: fe80::225:90ff:fed6:6ab0 \\
            Target MAC: 33:33:00:00:00:02, Target IP: ff02::2
        }
    \item \textbf{Router Advertisement: I, fe80::20c:fcff:fe00:f955, am a router.} \\
        { \small
            Sender MAC: 00:0c:fc:00:f9:55, Sender IP: fe80::20c:fcff:fe00:f955 \\
            Target MAC: 00:25:90:d6:6a:b0, Target IP: fe80::225:90ff:fed6:6ab0 \\
            Network prefix: 2001:db8::/64
        }
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDP: Neighbor Discovery Protocol}
Example neighbor discovery communication:

\bigskip

2001:db8::1 wants to communicate with 2001:db8::2.\footnote{
This can be simulated using \texttt{ndisc6 2001:db8::2 eth0}} \\

\begin{itemize}
    \item \textbf{Neighbor Solicitation for 2001:db8::2 from 2001:db8::1.} \\
        { \small
            Sender MAC: 00:15:5d:30:dd:1e, Sender IP: 2001:db8::1 \\
            Target MAC: 33:33:ff:00:00:02, Target IP: ff02::1:ff00:2
        }
    \item \textbf{Neighbor Advertisement: 2001:db8::2 is at 00:25:90:d6:6a:b0.} \\
        { \small
            Sender MAC: 00:25:90:d6:6a:b0, Sender IP: 2001:db8::2 \\
            Target MAC: 00:15:5d:30:dd:1e, Target IP: 2001:db8::1
        }
\end{itemize}

Retrieved information is cached (more complex state mechanism than with~ARP neighbors).
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDPMon}

\begin{itemize}
    \item \url{http://ndpmon.sourceforge.net/}
    \item tool created in 2006 at University of Lorraine
    \item sliiightly better maintained than Arpwatch
    \item latest version from 2013
    \item packaged in Debian-derived distributions and FreeBSD (\texttt{ndpmon})
    \item useful even in non-IPv6 networks!
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDPMon}

Detection features:
\begin{itemize}
    \item invalid Router Advertisements (RA)
    \item suspicious/unexpected RA
    \item IP takeover/collisions
    \item DAD DoS
    \item station activity: new, new address, changes
    \item flip-flop
    \item invalid Neighbor Advertisement (NA)
\end{itemize}

See \url{http://ndpmon.sourceforge.net/index.php?n=Doc.Alerts} for~full list.
\end{frame}

\begin{frame}
\frametitle{Monitoring IPv6 address space}
\framesubtitle{MAC--LLA--IPv6 pairing}

\begin{itemize}
    \item syslog/mails can be processed
    \item database of MAC and IPv6 (LLA and global) with timestamps
    \item overview of MAC and IPv6 usage (however, privacy extensions)
    \item add switch (even L2!) bridge tables: \\
        \textbf{device localization to switch \& port}
    \item very valuable network information
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDPMon examples}

\begin{block}{Example NDPMon command}
\begin{semiverbatim}
touch vlan48.dat
ndpmon -i vlan48 -f vlan48.cfg -g vlan48.dat
\end{semiverbatim}
\end{block}

Monitor NDP packets on interface vlan48 using configuration (with information
about permitted routers, their IPv6/link-layer addresses and advertised
networks) and store the XML database of observer nodes and routers on the IPv6
network in vlan48.dat (which must be created in advance).

\medskip

See \texttt{ndpmon(8)} for more information.
\end{frame}

\begin{frame}[fragile]
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDPMon examples}

\begin{semiverbatim}
Sep 30 16:45:46 ares1 NDPMon[24613]:  new station
    1c:6f:65:37:48:6a fe80:0:0:0:1e6f:65ff:fe37:486a

---

Subject: NDPMon_Security_Alert: new station
    1c:6f:65:37:48:6a fe80:0:0:0:1e6f:65ff:fe37:486a

Reason:  new station
MAC:     1c:6f:65:37:48:6a
Vendor:  Giga-ByteT
IPv6:    fe80:0:0:0:1e6f:65ff:fe37:486a
DNS:     <Unknown host>
Iface:   vlan48
\end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
\frametitle{Monitoring IPv6 address space}
\framesubtitle{NDPMon examples}

\begin{semiverbatim}
Sep 30 16:45:55 ares1 NDPMon[24613]:  new IP
    1c:6f:65:37:48:6a 2001:718:801:230:0:0:0:c3

---

Subject: NDPMon_Security_Alert: new IP
    1c:6f:65:37:48:6a 2001:718:801:230:0:0:0:c3

Reason:  new IP
MAC:     1c:6f:65:37:48:6a
Vendor:  Giga-ByteT
IPv6:    2001:718:801:230:0:0:0:c3
DNS:     minos.fi.muni.cz
Iface:   vlan48
\end{semiverbatim}
\end{frame}

% https://www.troopers.de/media/filer_public/cd/fb/cdfbe06d-c3b7-48a3-a697-9ae535646e2a/tr16_ipv6_sec_summit_hp_fhs_cwerny_v10.pdf

\begin{frame}[fragile]
\frametitle{Monitoring IPv6 address space}
\framesubtitle{Access switch NDP monitoring}

Basic access switch abilities (for HP comware):

\begin{block}{Block RA guard from non-authorized ports}
\begin{semiverbatim}
int te1/1/1
\ \ \ \ ipv6 nd raguard role router
int ra g1/0/1 to g1/0/48
\ \ \ \ ipv6 nd raguard role host
\end{semiverbatim}
\end{block}

For more info about RA Guard see \url{https://tools.ietf.org/html/rfc6105}.
\end{frame}

\begin{frame}[fragile]
\frametitle{Monitoring IPv6 address space}
\framesubtitle{Access switch NDP monitoring}

\begin{block}{Detect and block incorrect/malicious NDP packets}
\begin{semiverbatim}
ipv6 nd snooping enable global
ipv6 nd snooping enable link-local
vlan 48
\ \ \ \ ipv6 nd detection enable
int te1/1/1
\ \ \ \ ipv6 nd detection trust
\end{semiverbatim}
\end{block}

In-depth presentation on first-hop IPv6 security (RA guard, DHCPv6 snooping,
ND snooping, ND detection, IPv6 source guard):

\medskip

\url{https://www.troopers.de/events/ipv6-security-summit-2016/616_ipv6_first_hop_security_features_on_hp_devices/}
\end{frame}


\begin{frame}
\frametitle{Conclusion}

\begin{center}
Thank you for your attention.

\bigskip

Any questions?
\end{center}

\end{frame}

\end{document}

